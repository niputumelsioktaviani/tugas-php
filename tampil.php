<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Tampilan Hasil</title>
  </head>

  <body>
    <div class="p-3 mb-2 bg-dark bg-gradient text-white">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-8 border border-info mt-3 p-3">
                <h2 class="alert alert-info text-center fw-light">Tampilan Hasil Hitung Nilai</h2>
            
    <?php 
        $nama  = $_POST['nama'];
        $mapel = $_POST['mapel'];
        $uts   = $_POST['uts'];
        $uas   = $_POST['uas'];
        $tugas = $_POST['tugas'];

        $nilai_uts   = $uts * 0.35;
        $nilai_uas   = $uas * 0.5;
        $nilai_tugas = $tugas * 0.15;

        $nilai_total = $nilai_uts + $nilai_uas + $nilai_tugas;

        if ($nilai_total>=90 && $nilai_total<=100)
        {
            $nilaihuruf  = "A";
        }

        elseif ($nilai_total>70 && $nilai_total<90)
        {
            $nilaihuruf = "B";
        }

        elseif ($nilai_total>50 && $nilai_total<=70)
        {
            $nilaihuruf = "C";
        }

        elseif ($nilai_total<=50)
        {
            $nilaihuruf = "D";
        }

        else {
            $nilaihuruf = "E";
        }
        
        echo "<center>Nama Siswa/Siswi : <b><i>$nama</i></b></center> <br><br>";
        echo "<center>Mata Pelajaran   : <b><i>$mapel</i></b></center> <br><br> ";
        echo "<center>Nilai UTS        : <b>$uts</b></center> <br><br>";
        echo "<center>Nilai UAS        : <b>$uas</b></center> <br><br>";
        echo "<center>Total Nilai      : <b>$nilai_total</b></center> <br><br>";
        echo "<center>Grade            : <b>$nilaihuruf</b></center> <br>";
    ?>
    </div>
        </div>
            </div>
                </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

    
  </body>
</html>