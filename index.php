<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Tugas PHP</title>
       
  </head>

  <body>
  <div class="p-3 mb-2 bg-secondary bg-gradient text-white">
    <div class="container">
        <h2 class="alert alert-warning text-center fst-italic mt-3">Menghitung Nilai Total Siswa/Siswi</h2>
    </div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-8 border border-warning mt-3 p-3">
                <form action="tampil.php" method="post">
                    <div class="form-group mb-3">
                        <label for="nama" class="form-label">Nama Siswa/Siswi</label>
                        <input type="text" name="nama" class="form-control" placeholder="Masukkan Nama Lengkap Anda!" id="nama">
                    </div>

                    <div class="input-group mb-3">
                        <label class="input-group-text" for="inputGroupSelect01">Pilih Mata Pelajaran</label>
                            <select name="mapel" class="form-select" id="inputGroupSelect01">
                                <option selected>Choose...</option>
                                <option value="Matematika">Matematika</option>
                                <option value="Biologi">Biologi</option>
                                <option value="Pemrograman Web">Pemrograman Web</option>
                                <option value="Pemrograman Mobile">Pemrograman Mobile</option>
                                <option value="Rekayasa Perangkat Lunak">Rekayasa Perangkat Lunak</option>
                                <option value="Fisika">Fisika</option>
                                <option value="Struktur Data">Struktur Data</option>
                                <option value="Bahasa Inggris">Bahasa Inggris</option>
                                <option value="Interaksi Manusia dan Komputer">Interaksi Manusia dan Komputer</option>
                                <option value="Perencanaan SI">Perencanaan SI</option>
                            </select>
                        </div>

                        <div class="mb-3">
                            <label for="uts" class="form-label">Nilai UTS</label>
                            <input type="number" name="uts" class="form-control" id="uts">
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="uas" class="form-label">Nilai UAS</label>
                                    <input type="number" name="uas" class="form-control" id="uas">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="tugas" class="form-label">Nilai Tugas</label>
                                    <input type="number" name="tugas" class="form-control" id="tugas">
                                </div>
                            </div>
                        </div>
                        
                    <button type="submit" class="btn btn-warning">Hitung Nilai</button>
                </form>
            </div>
        </div>
    </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

  </body>
</html>